# where2.cash

An interactive map helping you engage with the Bitcoin Cash ecosystem.


# Updating

When changes have been made that need to be deployed to the webserver
run (in a docker that has npm and node):
```
npm ci
npm run build
```
which will update the 'public' folder.
