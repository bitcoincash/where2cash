// Load a library to handle events.
import EventEmitter from 'events';

// Load the leaflet map library.
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

// Extend leaflet with offline caching of tiles.
import 'leaflet.offline';

// Extend leaflet with toolbar support.
import 'leaflet-toolbar';
import 'leaflet-toolbar/dist/leaflet.toolbar.css';

// Extend leaflet with clustering support for markers.
import 'leaflet.markercluster';
import 'leaflet.markercluster/dist/MarkerCluster.css';
import 'leaflet.markercluster/dist/MarkerCluster.Default.css';

// Extend leaflet clustering with support for multiple layers.
import 'leaflet.featuregroup.subgroup';

// Extend leaflet with custom control support.
import 'leaflet-control-custom';
import '../style/Leaflet.Control.Custom.css';

// Extend leaflet with notfication support.
import 'leaflet-notifications';
import 'leaflet-notifications/css/leaflet-notifications.css';

export class MapManager
{
	// Data
	public static map: L.Map;
	private static markers: Record<number, L.Marker> = {};
	private static markerIcons: Record<string, L.Icon> = {};
	private static layers: Record<string, L.MarkerClusterGroup> = {};
	private static cluster: L.MarkerClusterGroup;
	private static tilesets: Record<string, L.TileLayer> = {};

	// Positions
	public static viewPosition: any = { latitude: 0, longitude: 0 };
	public static devicePosition: any = { latitude: 0, longitude: 0 };
	private static prevPosition: any = { latitude: 0, longitude: 0 };

	// Controls
	// NOTE: notifications is a subset of L.Control, but not fully.
	public static toggles: L.Control;
	public static notifications: any;
	public static deviceMarker: any;

	// Events
	public static events: EventEmitter;

	/**
	 * Initializes the map at the default scale and coordinates.
	 */
	public static async initialize(latitude: number = 60.115, longitude: number = 19.955, scale: number = 14): Promise<void>
	{
		// Configure map options.
		const options =
		{
			// Remove attribute to get a cleaner interface.
			attributionControl: false,

			// Values 2..18 allows continental views without repeats and legible streets.
			minZoom: 2,
			maxZoom: 18,
		};

		// Create a map of the world.
		this.map = L.map('where2cash', options);

		// Set the initial view coordinates and scale of the map.
		this.map.setView([latitude, longitude], scale);

		await this.initializeTilesets();
		await this.initializeLayers();
		await this.initializeControls();
		await this.initializeEvents();
		await this.initializeMarkerIcons();

		// Store the current map view position coordinates.
		await this.storeViewPosition();
	}

	public static async initializeLayers(): Promise<void>
	{
		// Create a cluster group.
		this.cluster = L.markerClusterGroup().addTo(this.map);

		// Create a layer groups.
		this.layers =
		{
			// @ts-ignore
			supported: L.featureGroup.subGroup(this.cluster),
			// @ts-ignore
			unverified: L.featureGroup.subGroup(this.cluster),
			// @ts-ignore
			unsupported: L.featureGroup.subGroup(this.cluster),
		};

		// Also add events when requested.
		if(window.location.hash === '#pizza')
		{
			// @ts-ignore
			this.layers['events'] = L.featureGroup.subGroup(this.cluster);
			this.layers['events'].addTo(this.map);
		}
		else
		{
			// Show supported merchants by default.
			this.layers.supported.addTo(this.map);

			// Don't show unsupported merchants by default.
			// this.layers.unsupported.addTo(this.map);

			// Don't show unverified merchants if requested.
			// this.layers.unverified.addTo(this.map);
		};
	}

	public static async initializeTilesets(): Promise<void>
	{
		//
		const tilesetUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}';

		// Set tile IDs to merge with tile options.
		const streetTiles    = { id: 'mapbox/streets-v11' };
		const satelliteTiles = { id: 'mapbox/satellite-streets-v11' };

		// Configure tile options.
		const options =
		{
			// Set up our access token.
			accessToken: 'pk.eyJ1IjoibW9uc3RlcmJpdGFyIiwiYSI6ImNqemdxOWJoajA3aGQzZG54ZjBuMDdrOHkifQ.8BkR9WVgDu3ZSAMFVepKOw',
		};

		// Create a list of available tilesets.
		this.tilesets =
		{
			// Add the mapbox.com tiles to this map.
			streets:   L.tileLayer(tilesetUrl, Object.assign({ tileSize: 512, zoomOffset: -1 }, streetTiles, options)),
			satellite: L.tileLayer(tilesetUrl, Object.assign({ tileSize: 512, zoomOffset: -1 }, satelliteTiles, options)),
		}

		// Add the streets tileset as the default for this map.
		this.tilesets.streets.addTo(this.map)
	}

	public static async initializeControls(): Promise<void>
	{
		// Configure notifications.
		const options =
		{
			timeout: 3000,
			position: 'bottomleft',
			closable: true,
			dismissable: true,
			className: 'modern',
		};

		// Add a layer control widget for the tilesets and marker layers.
		this.toggles = L.control.layers(this.tilesets, this.layers)

		// Add the layer toggles to the map.
		this.toggles.addTo(this.map);

		// Create notification control.
		// @ts-ignore
		this.notifications = L.control.notifications(options);

		// Add the notifications to the map.
		this.notifications.addTo(this.map);
	}

	public static async initializeEvents()
	{
		// Set up event emitter.
		this.events = new EventEmitter();

		// When the user drags the map, check if we have entered a new area.
		this.map.on('dragend', this.detectAreaChange.bind(this));
		this.map.on('zoomend', this.detectAreaChange.bind(this));

		// When the user zooms the map, check if we have entered a new area.
		this.map.on('dragend', this.storeViewPosition.bind(this));
		this.map.on('zoomend', this.storeViewPosition.bind(this));
	}

	private static async storeViewPosition(): Promise<void>
	{
		// Get the maps current position and scale.
		let position = this.map.getCenter();
		let scale = this.map.getZoom();

		// Store the new position.
		this.viewPosition =
		{
			latitude: position.lat,
			longitude: position.lng,
			scale: scale
		}
	}

	private static async detectAreaChange(event: any): Promise<void>
	{
		const dragPosition = this.map.getCenter();
		const dragDistance = dragPosition.distanceTo([this.prevPosition.latitude, this.prevPosition.longitude]);

		// If map was moved more than 2.5km from last area check..
		if(dragDistance > 2500)
		{
			// Set the new view position.
			this.prevPosition =
			{
				latitude: dragPosition.lat,
				longitude: dragPosition.lng
			};

			// Emit a changed view location event.
			this.events.emit('changedViewLocation');
		}
	}

	public static async updatePosition(position: any, scale: number = 14): Promise<void>
	{
		if(!position)
		{
			console.warn(arguments);
			return;
		};

		// Parse the new position.
		const { accuracy, latitude, longitude } = position.coords;

		// Store the current device position.
		this.devicePosition = { accuracy, latitude, longitude, scale };

		// Update device position marker and radius, restricted to 10 kilometer uncertainty.
		this.deviceMarker.setLatLng([latitude, longitude])
		this.deviceMarker.setRadius(Math.min(accuracy, 10000));

		// If tracking is enabled, fly to the new position.
		if(false)
		{
			await this.locatePosition();
		}
	}

	public static async locatePosition()
	{
		// TODO: Add some easy way to toggle tracking.
		// this.tracking = !this.tracking;

		// Set the view and device positions to the same location.
		this.viewPosition = this.devicePosition;

		// Scroll the map to the new position.
		this.map.flyTo([this.devicePosition.latitude, this.devicePosition.longitude], this.devicePosition.scale);
	}

	public static async enableLocation(): Promise<void>
	{
		// Check if geolocation is an available feature on the device.
		if('geolocation' in navigator)
		{
			// @ts-ignore
			const viewDeviceLocation = L.Toolbar2.Action.extend(
			{
				options:
				{
					toolbarIcon: { html: '&#8982;', tooltip: 'Go to device location'}
				},
				addHooks: this.locatePosition.bind(this)
			});

			// @ts-ignore
			new L.Toolbar2.Control({ position: 'topleft', className: 'deviceLocation', actions: [viewDeviceLocation] }).addTo(this.map);

			// Create a "you are here" marker, at position 0,0 with a 1 meter radius that will be updated when geoposition is available.
			this.deviceMarker = L.circle([0, 0], { color: 'red', fillColor: '#f03', fillOpacity: 0.33, radius: 1 });

			// Add the device marker to the map.
			this.deviceMarker.addTo(this.map);

			// Set up a minute-by-minute watcher for changes in position.
			navigator.geolocation.watchPosition(MapManager.updatePosition.bind(this), console.warn, { timeout: 60000, enableHighAccuracy: true });
		}
	}

	private static async initializeMarkerIcons(): Promise<void>
	{
		this.markerIcons =
		{
			blue:   await this.createStandardMarker('./images/marker-icon-2x-blue.png'),
			red:    await this.createStandardMarker('./images/marker-icon-2x-red.png'),
			green:  await this.createStandardMarker('./images/marker-icon-2x-green.png'),
			orange: await this.createStandardMarker('./images/marker-icon-2x-orange.png'),
			yellow: await this.createStandardMarker('./images/marker-icon-2x-yellow.png'),
			violet: await this.createStandardMarker('./images/marker-icon-2x-violet.png'),
			grey:   await this.createStandardMarker('./images/marker-icon-2x-grey.png'),
			black:  await this.createStandardMarker('./images/marker-icon-2x-black.png'),

			pizza:  await this.createStandardMarker('./images/marker-icon-2x-pizza.png'),
		}
	}

	/**
	* Utility function to create a standardized leaflet icon to be used as a map marker.
	*
	* @param icon   the relative URL of the image to use for the marker.
	*
	* @returns a leaflet icon with appropriate positioning and shadow.
	*/
	private static async createStandardMarker(icon: string): Promise<L.Icon>
	{
		// Set default shadow.
		const shadow = './images/marker-shadow.png';

		// Assemble icon parameters.
		const iconParameters: L.IconOptions =
		{
			iconUrl: icon,
			shadowUrl: shadow,
			iconSize: [25, 41],
			iconAnchor: [12,41],
			popupAnchor: [1, -34],
			shadowSize: [41, 41],
		};

		// Return an instance of a leaflet icon.
		return new L.Icon(iconParameters);
	}

	public static async removeMarker(id: number): Promise<void>
	{
		try
		{
			// Only remove the marker if it already exist.
			if(this.markers[id])
			{
				// For each layer.. (events, supported, unsupported, unverified)
				for(const layer in this.layers)
				{
					// If the marker is in this layer..
					if(this.layers[layer].hasLayer(this.markers[id]))
					{
						// Remove the marker from this layer.
						this.layers[layer].removeLayer(this.markers[id]);
					}
				}

				// Delete the marker.
				delete this.markers[id];
			}
		}
		catch(error)
		{
			console.warn(`Could not remove marker #${id}: `, error);
		}
	}

	public static async setMarker(id: number, latitude: number, longitude: number, layer: string, icon: string, label: string): Promise<void>
	{
		try
		{
			// Create a new instance of a marker.
			const marker = L.marker([latitude, longitude], { icon: this.markerIcons[icon] });

			// Attach the label to be shown as a popup when clicking the marker.
			marker.bindPopup(label);

			// Remove any existing marker.
			await this.removeMarker(id);

			// Add the new instance of the marker to the map on their respective layer.
			this.layers[layer].addLayer(marker);

			// Store the marker for future reference.
			this.markers[id] = marker;
		}
		catch(error)
		{
			console.error(`Could not set a new marker: `, error, arguments);
		}
	}
}

